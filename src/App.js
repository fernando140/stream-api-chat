import React from "react";
import { StreamChat } from "stream-chat";
import {
  Chat,
  Channel,
  ChannelHeader,
  MessageInput,
  MessageInputSmall,
  VirtualizedMessageList,
  Window,
} from "stream-chat-react";

import "stream-chat-react/dist/css/index.css";

const chatClient = StreamChat.getInstance("dz5f4d5kzrue");
const userToken =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiYnJva2VuLWNoZXJyeS0wIiwiZXhwIjoxNjIzMjk2MTUwfQ.5cCpxCfOVCLNJjlMExfxKwCEs_Z4g49lYa6Nx9-kPvI";

chatClient.connectUser(
  {
    id: "broken-cherry-0",
    name: "broken",
    image: "https://getstream.io/random_png/?id=broken-cherry-0&name=broken",
  },
  userToken
);

const channel = chatClient.channel("livestream", "spacex", {
  image: "https://goo.gl/Zefkbx",
  name: "SpaceX launch discussion",
});

const App = () => (
  <Chat client={chatClient} theme="livestream dark">
    <Channel channel={channel}>
      <Window>
        <ChannelHeader live />
        <VirtualizedMessageList />
        <MessageInput Input={MessageInputSmall} focus />
      </Window>
    </Channel>
  </Chat>
);

export default App;
